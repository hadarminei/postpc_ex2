package android.exercise.mini.calculator.app;

import java.io.Serializable;
import java.util.ArrayList;

public class SimpleCalculatorImpl implements SimpleCalculator {

  ArrayList<String> calculate = new ArrayList<>();

  @Override
  public String output() {
    if(calculate.isEmpty()) {
      return "0";
    }
    String result = "";
    //going through all characters added and creating the output string
    for(String s : calculate) {
      result += s;
    }
    return result;
  }

  @Override
  public void insertDigit(int digit) throws RuntimeException {
    if(digit < 0 || digit > 9) { //number out of boundaries
      throw new RuntimeException("Digit out of boundaries");
    }
    calculate.add(Integer.toString(digit));
  }

  @Override
  public void insertPlus() {
    // if the last character was also an operation only the first one should show
    if(calculate.get(calculate.size() - 1).equals("+") || calculate.get(calculate.size() - 1).equals("-")) {
      return;
    }

    if(calculate.isEmpty()) {
      calculate.add("0");
    }
    calculate.add("+");
  }

  @Override
  public void insertMinus() {
    // if the last character was also an operation only the first one should show
    if(calculate.get(calculate.size() - 1).equals("+") || calculate.get(calculate.size() - 1).equals("-")) {
      return;
    }

    if(calculate.isEmpty()) {
      calculate.add("0");
    }
    calculate.add("-");
  }

  @Override
  public void insertEquals() {
    //nothing to calculate
    if(calculate.isEmpty()) {
      return;
    }
    String cur_num = "";
    String prev_op = "+";
    int result = 0;
    // go through all numbers and operation inside the calculation board
    for(int i = 0; i < calculate.size(); i++) {
      //when getting to an operation sign we need to calculate the previous number with the previous operation
      if(calculate.get(i).equals("+") || calculate.get(i).equals("-")) {
        if (prev_op.equals("+"))
          result += Integer.parseInt(cur_num);
        if (prev_op.equals("-"))
          result -= Integer.parseInt(cur_num);
        prev_op = calculate.get(i);
        cur_num = "";
      }
      else {
        cur_num += calculate.get(i);
      }
    }
    // handling the end of the expression
    if (prev_op.equals("+"))
      result += Integer.parseInt(cur_num);
    if (prev_op.equals("-"))
      result -= Integer.parseInt(cur_num);
    calculate.clear();
    calculate.add(Integer.toString(result));
  }

  @Override
  public void deleteLast() {
    //nothing to delete
    if(calculate.isEmpty()) {
      return;
    }
    calculate.remove(calculate.size() - 1);
  }

  @Override
  public void clear() {
    calculate.clear();
  }

  @Override
  public Serializable saveState() {
    CalculatorState state = new CalculatorState(calculate);
    return state;
  }

  @Override
  public void loadState(Serializable prevState) {
    if (!(prevState instanceof CalculatorState)) {
      return; // ignore
    }
    CalculatorState casted = (CalculatorState) prevState;
    calculate = casted.getCalculate();
  }

  private static class CalculatorState implements Serializable {
    ArrayList<String> state_calculate;

    // constructor for state
    CalculatorState(ArrayList<String> prev_calculate) {
      state_calculate = new ArrayList<>(prev_calculate);
    }

    ArrayList<String> getCalculate() {
      return state_calculate;
    }
  }
}
